import tensorflow as tf

# Model parameters
W = tf.Variable([0.3], tf.float32)
b = tf.Variable([-0.3], tf.float32)

# Inputs and Outputs
x = tf.placeholder(tf.float32)
y = tf.placeholder(tf.float32)
linearModel = (W * x) + b

# Loss
squaredDelta = tf.square(linearModel - y)
error = tf.reduce_sum(squaredDelta)

optimizer = tf.train.GradientDescentOptimizer(0.01)
train = optimizer.minimize(error)

init = tf.global_variables_initializer()
sess = tf.Session()
sess.run(init)

for i in range(1000):
    sess.run(train, {x: [1, 2, 3, 4], y: [0, -1, -2, -3]})
    print(sess.run([W, b]))
    print(error)
