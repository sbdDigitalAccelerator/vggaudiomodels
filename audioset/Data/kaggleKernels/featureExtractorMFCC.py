import librosa

SAMPLE_RATE = 44100


def extractMFCC(fileName):
    wav, _ = librosa.core.load(fileName, sr=SAMPLE_RATE)
    wav = wav[:2 * 44100]
    mfcc = librosa.feature.mfcc(wav, sr=SAMPLE_RATE, n_mfcc=40)
    # print(str(mfcc.shape))
    return mfcc
