from testAudioExtraction import getRawAudioFileNames
from Data.kaggleKernels.featureExtractorMFCC import extractMFCC

STATIC_AUDIO_RESOURCE_PATH = "/home/sanket/Documents/Data/SoundData/audio_train/"


def printMFCCFeatures():
    filenames = getRawAudioFileNames()
    i = 0
    while (i < len(filenames)):
        try:
            print(extractMFCC(STATIC_AUDIO_RESOURCE_PATH + filenames[i]))
            i = i + 1
        except:
            print("File threw an exception :: " + filenames[i])
            i = i + 1

printMFCCFeatures()
