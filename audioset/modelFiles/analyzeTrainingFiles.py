import testAudioExtraction
import tensorflow as tf
import vggish_input
import vggish_params
import vggish_postprocess
import vggish_slim

from vggish_inference_demo import run_inference

FILEPREFIX = "/home/sanket/Documents/Data/SoundData/audio_train/"


def printFileNames():
    i = 0
    fileNames = testAudioExtraction.getReducedTrainingSetNames()
    while (i < len(fileNames)):
        print(" i = " + str(i) + " filename = " + fileNames[i])
        waveFileName = FILEPREFIX + fileNames[i]
        waveFileName = waveFileName.replace("\n", "")
        run_inference(waveFileName)
        i = i + 1


printFileNames()
