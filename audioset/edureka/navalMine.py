
# read the dataset
# define the features
# encode the dependent variables
# divide the dataset - train, test
# define tf datastructures for holding features, labels etc
# implement the model
# train the model with train data
# calculate the error
# reduce the error
# make predictions on the test data once model converges

import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np
import pandas as pd

from sklearn.preprocessing import LabelEncoder
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split

#Reading the dataset
def readDataset():
    df = pd.read_csv("/home/sanket/Documents/workspace/models/research/audioset/Data/sonar.all-data.csv", )