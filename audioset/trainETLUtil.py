import csv
import sys
import traceback
import numpy as np

def getTrainingFileNames():
    trainFileList = "Data/groundTruth.csv"
    staticEmbeddingsDir = "/home/sanket/Documents/Data/SoundData/audio_train/vggEncoding/formattedInput/"
    fileNames = []
    with open(trainFileList, "rt") as csvFile:
        trainFileReader = csv.reader(csvFile, delimiter=",")
        for row in trainFileReader:
            try:
                currentRow = row[0]
                currentEnc = currentRow.replace(".wav",".enc")
                fileName = staticEmbeddingsDir + currentEnc
                fileNames.append(fileName)
            except:
                print(row[0] + " threw an exception.")
                traceback.print_exc(file=sys.stdout)
                continue
    return fileNames