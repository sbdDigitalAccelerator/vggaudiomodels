from __future__ import absolute_import, division, print_function
import tensorflow as tf
import numpy as np
from sklearn.cross_validation import StratifiedKFold
from tensorflow import keras
from tensorflow.keras.layers import Dense
from keras.wrappers.scikit_learn import KerasClassifier
from testAudioExtraction import readEncodedArrays
from Data.dataSplits.dataPipeline import getTrainData
from Data.dataSplits.dataPipeline import getTestData
from Data.dataSplits.dataPipeline import getTestingFileNames
from Data.dataSplits.dataPipeline import getMFCCTrainData
from Data.dataSplits.dataPipeline import getMFCCTestData
from Data.dataSplits.dataPipeline import getTrainingMFCCData
from Data.dataSplits.dataPipeline import getTestingMFCCData
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler
import vggish_slim

from keras.layers import (Convolution2D, GlobalAveragePooling2D, BatchNormalization, Flatten,
                          GlobalMaxPool2D, MaxPool2D, concatenate, Activation)
from keras.utils import Sequence, to_categorical
from keras import backend as K

import librosa
import numpy as np
import pandas as pd
import scipy
from keras import losses, models, optimizers
from keras.activations import relu, softmax
from keras.callbacks import (EarlyStopping, LearningRateScheduler,
                             ModelCheckpoint, TensorBoard, ReduceLROnPlateau)
from keras.layers import (Convolution1D, Dense, Dropout, GlobalAveragePooling1D,
                          GlobalMaxPool1D, Input, MaxPool1D, concatenate)
from keras.utils import Sequence, to_categorical
from Data.dataSplits.dataPipeline import prepare_data

class_names = ["Meow", "Bark", "Cowbell"]
slim = tf.contrib.slim
PREDICTION_FOLDER = "/home/sanket/Data/soundData/"


class Config(object):
    def __init__(self,
                 sampling_rate=16000, audio_duration=2, n_classes=41,
                 use_mfcc=False, n_folds=10, learning_rate=0.0001,
                 max_epochs=50, n_mfcc=20):
        self.sampling_rate = sampling_rate
        self.audio_duration = audio_duration
        self.n_classes = n_classes
        self.use_mfcc = use_mfcc
        self.n_mfcc = n_mfcc
        self.n_folds = n_folds
        self.learning_rate = learning_rate
        self.max_epochs = max_epochs

        self.audio_length = self.sampling_rate * self.audio_duration
        if self.use_mfcc:
            self.dim = (self.n_mfcc, 1 + int(np.floor(self.audio_length / 512)), 1)
        else:
            self.dim = (self.audio_length, 1)


def create_model(input_data, input_labels):
    embeddings = vggish_slim.define_vggish_slim(False)
    print("Input data shape = " + str(embeddings.shape))
    model = keras.Sequential()
    model.add(keras.layers.Dense(12, input_dim=128, activation='relu'))
    model.add(keras.layers.Dense(3, activation='softmax'))
    model.compile(optimizer=keras.optimizers.Adam(), loss='sparse_categorical_crossentropy', metrics=['accuracy'])
    return model


def create_model2(input_data, input_labels):
    # This returns a tensor
    embeddings = vggish_slim.define_vggish_slim(False)

    # Add a fully connected layer with 100 units.
    num_units = 100
    fc = slim.fully_connected(embeddings, num_units)

    # Add a classifier layer at the end, consisting of parallel logistic
    # classifiers, one per class. This allows for multi-class tasks.
    logits = slim.fully_connected(
        fc, 3, activation_fn=None, scope='logits')

    model = keras.Sequential()
    model.add(keras.layers.Dense(50, input_shape=(128,), activation='relu'))
    model.add(keras.layers.Dense(3, activation='softmax'))
    model.compile(optimizer=keras.optimizers.Adam(), loss='sparse_categorical_crossentropy', metrics=['accuracy'])
    return model


def create_model3():
    model = keras.Sequential()
    model.add(Dense(140, input_dim=128, activation="relu"))
    model.add(Dense(120, activation="relu"))
    model.add(Dense(41, activation="softmax"))

    # Compile the model
    # model.compile(optimizer=keras.optimizers.Adam(), loss='sparse_categorical_crossentropy', metrics=['accuracy'])
    model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])
    return model


# Layer	Activation shape	# Weights	# Multiplies
# Input	(25, 64, 1)	0	0
# Conv2D(7x7, 1, 100)	(25. 64. 100)	4.9K	7.8M
# MaxPool2D(3x3, 2x2)	(13, 32, 100)	0	0
# Conv2D(5x5, 1, 150)	(13, 32, 150)	375K	156M
# MaxPool2D(3x3, 2x2)	(7, 16, 150)	0	0
# Conv2D(3x3, 1, 200)	(7, 16, 200)	270K	30.2M
# ReduceMax	(1, 1, 200)	0	0
# Softmax	(41,)	8.2K	8.2K
# Total

def create_model4():
    model = keras.Sequential
    model.add(Dense(140, input_dim=128, activation="relu"))
    model.add(keras.layers.Conv2D(kernel_size=25, strides=64, ))
    return model


def create_model5():
    model = keras.Sequential()
    model.add(Dense(140, input_dim=17, activation="relu"))
    model.add(Dense(120, activation="relu"))
    model.add(Dense(41, activation="softmax"))

    # Compile the model
    model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])
    return model


def debugModelShape(i, j, input_data, input_labels):
    print(" i = " + str(i) + " j = " + str(j) + " featuresShape = " + str(input_data.shape) + " labelsShape = " + str(
        input_labels.shape))


def train_network():
    print("Commencing training")
    # (train_data, train_labels) = readEncodedArrays()
    (train_data, train_labels) = getTrainData()

    model = create_model3()
    model.fit(train_data, train_labels, epochs=50)
    filepath = "/home/sanket/Documents/pythonWorkspace/audioset/enhancedModels/enModel_exp2.h5"
    model.save(filepath)


def test_network(modelPath, test_data, test_labels):
    print("Commencing Testing")
    model = keras.models.load_model(modelPath)
    test_loss, test_acc = model.evaluate(test_data, test_labels)
    print("Test accuracy = " + str(test_acc))


def getLabelString(i):
    if (i == 0):
        return "Meow"
    elif (i == 1):
        return "Bark"
    else:
        return "Cowbell"


def get_2d_conv_model(config):
    nclass = config.n_classes

    inp = Input(shape=(config.dim[0], config.dim[1], 1))
    x = Convolution2D(32, (4, 10), padding="same")(inp)
    x = BatchNormalization()(x)
    x = Activation("relu")(x)
    x = MaxPool2D()(x)

    x = Convolution2D(32, (4, 10), padding="same")(x)
    x = BatchNormalization()(x)
    x = Activation("relu")(x)
    x = MaxPool2D()(x)

    x = Convolution2D(32, (4, 10), padding="same")(x)
    x = BatchNormalization()(x)
    x = Activation("relu")(x)
    x = MaxPool2D()(x)

    x = Convolution2D(32, (4, 10), padding="same")(x)
    x = BatchNormalization()(x)
    x = Activation("relu")(x)
    x = MaxPool2D()(x)

    x = Flatten()(x)
    x = Dense(64)(x)
    x = BatchNormalization()(x)
    x = Activation("relu")(x)
    out = Dense(nclass, activation=softmax)(x)

    model = models.Model(inputs=inp, outputs=out)
    opt = optimizers.Adam(config.learning_rate)

    model.compile(optimizer=opt, loss=losses.categorical_crossentropy, metrics=['acc'])
    return model


def translateOneHotString(label):
    if (label == "100"):
        return "Meow"
    elif (label == "010"):
        return "Bark"
    else:
        return "Cowbell"


def test_network_predict(modelPath, test_data, test_labels):
    model = keras.models.load_model(modelPath)
    predictions = model.predict(test_data)
    filesList = getTestingFileNames()
    print("Total number of test samples = " + str(len(filesList)))

    i = 0
    j = 0
    for index, row in test_labels.iterrows():
        predictedLabel = np.argmax(predictions[i])
        predictedLabel = getLabelString(predictedLabel)
        try:
            actualLabel = str(row['Meow']) + str(row['Bark']) + str(row['Cowbell'])
            translatedLabel = translateOneHotString(actualLabel)

            if (predictedLabel != translatedLabel):
                print("Index = " + str(index) + " j = " + str(j) + " fileName = " + filesList[i].replace("\n",
                                                                                                         "") + " Predicted Label = " + str(
                    predictedLabel) + " " + str(
                    predictions[i]) + " argMax = " + str(np.argmax(
                    predictions[i])) + " actual Label = " + actualLabel + " translatedLabel = " + translatedLabel)
                # print("Test label = " + str(test_labels.get_value(index)))
                print(" Index = " + str(index) + " i = " + str(i) + " label = " + str(test_labels.iloc[i]))
                j = j + 1

            i = i + 1
        except:
            i = i + 1
    print(" total rows iterated over = " + str(i))


def train_networkMfcc(train_data, train_labels):
    print("Commencing training")
    # (train_data, train_labels) = readEncodedArrays()
    # (train_data, train_labels) = getTrainingMFCCData()

    model = create_model5()
    model.fit(train_data, train_labels, epochs=80)
    filepath = "/home/sanket/Documents/pythonWorkspace/audioset/enhancedModels/enModel_exp2_mfcc.h5"
    model.save(filepath)


def train_network_mfcc(train_data, train_labels):
    print("Commencing training")
    config = Config(sampling_rate=44100, audio_duration=2, n_folds=10,
                    learning_rate=0.001, use_mfcc=True, n_mfcc=40)
    model = get_2d_conv_model(config)
    model.fit(train_data, train_labels, epochs=100)
    filepath = "/home/sanket/Documents/pythonWorkspace/audioset/enhancedModels/enModelMfcc2DConv.h5"
    model.save(filepath)


config = Config(sampling_rate=44100, audio_duration=2, n_folds=10,
                learning_rate=0.001, use_mfcc=True, n_mfcc=40)

train = pd.read_csv("/home/sanket/Documents/Data/SoundData/train.csv")
test = pd.read_csv("/home/sanket/Documents/Data/SoundData/sample_submission.csv")

X_train = prepare_data(train, config, '/home/sanket/Documents/Data/SoundData/audio_train/')
X_test = prepare_data(test, config, '/home/sanket/Documents/Data/SoundData/audio_test/')
y_train = to_categorical(train.label_idx, num_classes=41)

# mean = np.mean(X_train, axis=0)
# std = np.std(X_train, axis=0)
#
# X_train = (X_train - mean) / std
# X_test = (X_test - mean) / std
#
# LABELS = list(train.label.unique())
#
# skf = StratifiedKFold(train.label_idx, n_folds=config.n_folds)
# for i, (train_split, val_split) in enumerate(skf):
#     K.clear_session()
#     X, y, X_val, y_val = X_train[train_split], y_train[train_split], X_train[val_split], y_train[val_split]
#     checkpoint = ModelCheckpoint('best_%d.h5' % i, monitor='val_loss', verbose=1, save_best_only=True)
#     early = EarlyStopping(monitor="val_loss", mode="min", patience=5)
#     tb = TensorBoard(log_dir='./logs/' + PREDICTION_FOLDER + '/fold_%i' % i, write_graph=True)
#     callbacks_list = [checkpoint, early, tb]
#     print("#" * 50)
#     print("Fold: ", i)
#     model = get_2d_conv_model(config)
#     history = model.fit(X, y, validation_data=(X_val, y_val), callbacks=callbacks_list,
#                         batch_size=64, epochs=config.max_epochs)
#     model.load_weights('best_%d.h5' % i)
#
#     # Save train predictions
#     predictions = model.predict(X_train, batch_size=64, verbose=1)
#     np.save(PREDICTION_FOLDER + "/train_predictions_%d.npy" % i, predictions)
#
#     # Save test predictions
#     predictions = model.predict(X_test, batch_size=64, verbose=1)
#     np.save(PREDICTION_FOLDER + "/test_predictions_%d.npy" % i, predictions)
#
#     # Make a submission file
#     top_3 = np.array(LABELS)[np.argsort(-predictions, axis=1)[:, :3]]
#     predicted_labels = [' '.join(list(x)) for x in top_3]
#     test['label'] = predicted_labels
#     test[['label']].to_csv(PREDICTION_FOLDER + "/predictions_%d.csv" % i)


(train_data, train_labels) = getMFCCTrainData()
train_network_mfcc(train_data, train_labels)
test_data, test_labels = getMFCCTestData()
test_network("/home/sanket/Documents/pythonWorkspace/audioset/enhancedModels/enModelMfcc2DConv.h5", test_data,
             test_labels)

(train_data, train_labels) = getTrainingMFCCData()

scaler = StandardScaler()
scaledTrainData = scaler.fit_transform(train_data)

train_networkMfcc(scaledTrainData, train_labels)
test_data, test_labels = getTestingMFCCData()
scaledTestData = scaler.transform(test_data)
test_network("/home/sanket/Documents/pythonWorkspace/audioset/enhancedModels/enModel_exp2_mfcc.h5", scaledTestData,
             test_labels)

test_network_predict("/home/sanket/Documents/workspace/models/research/audioset/enhancedModels/enModel.h5", test_data,
                     test_labels)

test_data.to_csv("/home/sanket/Documents/workspace/models/research/audioset/Data/testData/testData.csv")
test_labels.to_csv("/home/sanket/Documents/workspace/models/research/audioset/Data/testData/testLabels.csv")

# PARK ZONE
# def testModelShape():
#     (train_data, train_labels) = readEncodedArrays()
#
#     i = 0
#     j = 0
#     while (i < len(train_data)):
#         while (j < len(train_labels)):
#             debugModelShape(i, j, train_data[i], train_labels[j])
#             j = j + 1
#         i = i + 1
#         j = 0
#
#
# testModelShape()
