import csv
import sys
import traceback
import numpy as np
import tensorflow as tf

from vggish_input import wavfile_to_examples
from vggish_inference_demo import run_inference
from vggish_inference_demo import writeToDisk

SEPARATOR = "======================================================================"
THIN_SEPARATOR = "----------------------------------------------------------------------"
staticAudioDataDir = "/home/sanket/Documents/Data/SoundData/audio_train"
staticEmbeddingsDir = "/home/sanket/Documents/Data/SoundData/audio_train/vggEncoding/formattedInput/"
featureArrayDir = "/home/sanket/Documents/Data/SoundData/audio_train/featuresDir/"
featureReducedDir = "/home/sanket/Documents/Data/SoundData/audio_train/featuresDir/reducedDataDir/"


def getTrainingFileNames():
    trainFileList = "/home/sanket/Documents/Data/SoundData/groundTruth.csv"
    staticEmbeddingsDir = "/home/sanket/Documents/Data/SoundData/audio_train/vggEncoding/formattedInput/"
    fileNames = []
    with open(trainFileList, "rt") as csvFile:
        trainFileReader = csv.reader(csvFile, delimiter=",")
        for row in trainFileReader:
            try:
                currentRow = row[0]
                currentEnc = currentRow.replace(".wav", ".enc")
                fileName = staticEmbeddingsDir + currentEnc
                fileNames.append(fileName)
            except:
                print(row[0] + " threw an exception.")
                traceback.print_exc(file=sys.stdout)
                continue
    return fileNames


def getRawAudioFileNames():
    trainFileList = "/home/sanket/Documents/pythonWorkspace/audioset/Data/groundTruth.csv"
    fileNames = []
    with open(trainFileList, "rt") as csvFile:
        trainFileReader = csv.reader(csvFile, delimiter=",")
        for row in trainFileReader:
            try:
                currentRow = row[0]
                # fileName = staticAudioDataDir + "/" + currentRow
                fileNames.append(currentRow)
            except:
                print(row[0] + " threw an exception.")
                traceback.print_exc(file=sys.stdout)
                continue
    return fileNames


def getReducedTrainingSetNames():
    trainFileList = "/home/sanket/Documents/Data/SoundData/reducedDataGuide.txt"
    fileNames = []
    i = 0
    dataGuide = open(trainFileList, "r")
    dataLine = dataGuide.readline()
    while dataLine:
        fileNames.append(dataLine)
        i = i + 1
        dataLine = dataGuide.readline()
    return fileNames


def readEncodedTrainingFiles(path):
    encodingFile = open(path, 'r')
    encodings = encodingFile.read()
    features = np.array(encodings.split('|')[0])
    labels = np.array(encodings.split('|')[1])
    return features, labels


def readTrainingDataSet():
    features = np.zeros((150, 96, 64), dtype=float)
    labels = np.zeros((150, 41), dtype=float)
    # fileNames = getTrainingFileNames()
    fileNames = getReducedTrainingSetNames()
    i = 0
    while (i < 150):
        fileName = fileNames[i]
        features[i] = np.array(readEncodedTrainingFiles(fileName)[0])
        labels[i] = np.array(readEncodedTrainingFiles(fileName)[1])
        print(i)
        i = i + 1
    return features, labels


def prepareFeatures():
    fileNames = getReducedTrainingSetNames()
    i = 0
    while (i < len(fileNames)):
        fileName = fileNames[i]

        audioFileName = staticAudioDataDir + "/" + fileName
        audioFileName = audioFileName.replace("\n", "")
        featuresFileName = featureReducedDir + "/" + fileName
        featuresFileName = featuresFileName.replace(".wav", ".txt").replace("\n", "")
        print(fileName)
        features = wavfile_to_examples(audioFileName)
        np.save(featuresFileName, features)
        i = i + 1
        # print(features)
    print(i)


def readTrainingArrays():
    batchLength = 54
    features = np.empty((batchLength, 96, 64), dtype=float)
    # features = []
    labels = np.empty((batchLength, 3), dtype=int)
    # labels = []
    featuresDir = "/home/sanket/Documents/Data/SoundData/audio_train/featuresDir/reducedDataDir/"
    labelsDir = "/home/sanket/Documents/Data/SoundData/reducedLabelsSet/"
    # rawAudioFileNames = getRawAudioFileNames()
    rawAudioFileNames = getReducedTrainingSetNames()
    i = 0
    j = 0
    errors = 0
    requiredSize = 0
    while (i < batchLength):
        try:
            fileName = rawAudioFileNames[i]
            fileName = fileName.replace("\n", "")
            featuresFileName = featuresDir + fileName + ".npy"
            featuresFileName = featuresFileName.replace(".wav", ".txt")
            labelsFileName = labelsDir + fileName.replace(".wav", ".lbl")
            labelFile = open(labelsFileName, "r")
            label = labelFile.read().replace("[", "").replace("]", "")
            featuresArr = np.load(featuresFileName)
            labelsArr = np.fromstring(label, dtype=int, sep=',')

            k = 0
            while (k < len(featuresArr)):
                features[j] = featuresArr[k]
                labels[j] = labelsArr
                k = k + 1
                j = j + 1

            i = i + 1
        except:
            # print("featuresArr shape = " + str(featuresArr.shape))
            # print("labelsArr shape = " + str(labelsArr.shape))
            errors = errors + 1
            i = i + 1
            continue
    # print("Num errors = " + str(errors))
    # print("features = " + str(len(features)))
    return features, labels


def readEncodedArrays():
    batchLength = 585
    features = np.empty((batchLength, 128,), dtype=float)
    labels = np.empty((batchLength, 3), dtype=int)
    featuresDir = "/home/sanket/Documents/Data/SoundData/embeddingsDir/"
    labelsDir = "/home/sanket/Documents/Data/SoundData/reducedLabelsSet/"
    rawAudioFileNames = getReducedTrainingSetNames()
    i = 0
    j = 0
    errors = 0
    requiredSize = 0
    while (i < batchLength):
        try:
            fileName = rawAudioFileNames[i]
            fileName = fileName.replace("\n", "")
            featuresFileName = featuresDir + fileName + ".npy"
            featuresFileName = featuresFileName.replace(".wav", ".emb")
            labelsFileName = labelsDir + fileName.replace(".wav", ".lbl")
            labelFile = open(labelsFileName, "r")
            label = labelFile.read().replace("[", "").replace("]", "")
            featuresArr = np.load(featuresFileName)
            labelsArr = np.fromstring(label, dtype=int, sep=',')

            # print("Correct file = " + fileName)
            # print("Features array shape = " + str(featuresArr.shape))
            # print("Labels array shape = " + str(labelsArr.shape))

            k = 0
            while (k < len(featuresArr)):
                features[j] = featuresArr[k]
                print("Debug statement - featuresArr.shape = " + str(featuresArr[k].shape))
                print("Debug statement - labelsArr.shape = " + str(labelsArr.shape))
                labels[j] = labelsArr
                k = k + 1
                j = j + 1

            i = i + 1
        except:
            # print("featuresArr shape = " + str(featuresArr.shape))
            # print("labelsArr shape = " + str(labelsArr.shape))
            # print("Error for file = " + fileName)
            errors = errors + 1
            i = i + 1
            continue
    # print("Num errors = " + str(errors))
    # print("features = " + str(len(features)))
    return features, labels


# waveFile = "/home/sanket/Documents/Data/SoundData/audio_train/d49d8904.wav"
# print(wavfile_to_examples(waveFile))
print(SEPARATOR)

trainFileList = "Data/groundTruth.csv"


def addSmallValue(labelArr):
    localArr = np.array(labelArr)
    for i in range(len(localArr)):
        localArr[i] = localArr[i] + 0.0000000001
    return localArr


def readTrainingSet():
    with open(trainFileList, "rt") as csvFile:
        trainFileReader = csv.reader(csvFile, delimiter=",")
        for row in trainFileReader:
            print(SEPARATOR)
            try:
                currentRow = row[0]
                currentEnc = currentRow.replace(".wav", ".enc")
                print("Currently processed file =" + currentEnc)
                print(THIN_SEPARATOR)
                fileName = staticEmbeddingsDir + currentEnc
                readEncodedTrainingFiles(fileName)
                print(THIN_SEPARATOR)
            except:
                print(row[0] + " threw an exception.")
                traceback.print_exc(file=sys.stdout)
                continue


def runTestSet():
    audioFilePrefix = "/home/sanket/Documents/Data/SoundData/audio_train/"
    embeddingsSavePath = "/home/sanket/Documents/Data/SoundData/embeddingsDir/"
    fileNames = getReducedTrainingSetNames()
    i = 0
    while (i < len(fileNames)):
        inputFileName = audioFilePrefix + fileNames[i];
        inputFileName = inputFileName.replace("\n", "")

        saveFileName = embeddingsSavePath + fileNames[i].replace("\n", "")
        saveFileName = saveFileName.replace(".wav", ".emb")
        print(audioFilePrefix + fileNames[i])
        try:
            embedding_batch = run_inference(inputFileName)
            print(embedding_batch)
            print("Embedding batch Shape => " + str(embedding_batch.shape))
            print("File will be saved to " + saveFileName)
            np.save(saveFileName, embedding_batch)
            i = i + 1
        except:
            i = i + 1
            continue

# readTrainingArrays()
# prepareFeatures()

# runTestSet()
#print(readEncodedArrays())

# readTrainingDataSet()
# print(len(getTrainingFileNames()))
# print(readEncodedTrainingFiles("/home/sanket/Documents/Data/SoundData/audio_train/vggEncoding/formattedInput/bce5ed32.enc")[1])
# readTrainingDataSet()


# readTrainingSet()
# print(getTrainingFileNames())
# print("End of execution.")
# print(SEPARATOR)

#readEncodedTrainingFiles()


# PARK ZONE
# content = run_inference(staticAudioDataDir + "/" + row[0])
# content = wavfile_to_examples(staticAudioDataDir + "/" + row[0])
# print("Printing content")
# print(content)
# print(SEPARATOR)
# writeToDisk(staticAudioDataDir + "/vggEncoding/" + currentRow.replace(".wav", ".enc"), str(content))
# content = run_inference(staticAudioDataDir + "/" + row[0]);


# print("Features shape = ")
# print(features.shape)
# print("Labels shape = ")
# print(labels.shape)
#
# print("FeaturesArr shape = ")
# print(featuresArr.shape)
# print("FeaturesArr length = " + str(len(featuresArr)))
# print("LabelsArr length = " + str(len(labelsArr)))
#
# requiredSize += len(featuresArr)
# if(len(features) == 0):
#     print("This works")
# else:
#     print(len(features))

# features=np.concatenate(features,featuresArr)
# features[j] = featuresArr
# labels[j] = labelsArr
# labels.append(labelsArr)
# labels=np.concatenate(labels,labelsArr)
