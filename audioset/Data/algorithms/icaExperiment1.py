"""
=====================================
Blind source separation using FastICA
=====================================

An example of estimating sources from noisy data.

:ref:`ICA` is used to estimate sources given noisy measurements.
Imagine 3 instruments playing simultaneously and 3 microphones
recording the mixed signals. ICA is used to recover the sources
ie. what is played by each instrument. Importantly, PCA fails
at recovering our `instruments` since the related signals reflect
non-Gaussian processes.

"""
print(__doc__)

import os
import wave
import pylab
import matplotlib

import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
from scipy.io import wavfile
from scipy import vstack

from sklearn.decomposition import FastICA, PCA

###############################################################################

STATIC_AUDIO_PATH = "/home/sanket/Documents/Data/SoundData/audio_train/"

matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

# read data from wav files
sourceFile1 = STATIC_AUDIO_PATH + "8f5cdb59.wav"
sourceFile2 = STATIC_AUDIO_PATH + "b753a444.wav"

destFile1 = STATIC_AUDIO_PATH + "8f5cdb59_Sep.wav"
destFile2 = STATIC_AUDIO_PATH + "b753a444_Sep.wav"

mixedFile1 = STATIC_AUDIO_PATH + "mixedSignal.wav"

newSourceFile = STATIC_AUDIO_PATH + "mergedAudio.wav"
merged_rate1, mergedArr = wavfile.read(newSourceFile)

print('merged_rate1', merged_rate1)
print(str(mergedArr.shape))
print(mergedArr)

sample_rate1, samples1 = wavfile.read(sourceFile1)
sample_rate2, samples2 = wavfile.read(sourceFile2)

print('sample_rate1', sample_rate1)
print('sample_rate2', sample_rate2)

print(str(samples1.shape))
print(str(samples2.shape))

diff = len(samples2) - len(samples1)
print(diff)

newSamples1 = np.zeros(diff)
newSamples1 = np.append(samples1, newSamples1)
# print(newSamples1)
# print(str(newSamples1.shape))

S = np.c_[newSamples1, samples2]

print("Shape of S = " + str(S.shape))
print("S = " + str(S))

ica = FastICA(n_components=2)
S_ = ica.fit_transform(mergedArr)  # Reconstruct signals

print('original signal=', S)
print('recovered signal=', S_)
print('extracted signal1', S_[:, 0])
print('extracted signal2', S_[:, 1])

# write data to wav files
scaled1 = np.int16(S_[:, 0] / np.max(np.abs(S_[:, 0])) * 32767)
wavfile.write(destFile1, sample_rate1, scaled1)

scaled2 = np.int16(S_[:, 1] / np.max(np.abs(S_[:, 1])) * 32767)
wavfile.write(destFile2, sample_rate2, scaled2)

mixedScale = np.int16(S / np.max(np.abs(S_[:, 0])) * 32767)
wavfile.write(mixedFile1, sample_rate1, mixedScale)

###############################################################################
# Plot results

# pylab.figure(num=None, figsize=(10, 10))
#
# pylab.subplot(411)
# pylab.title('(received signal 1)')
# pylab.xlabel('Time (s)')
# pylab.ylabel('Sound amplitude')
# pylab.plot(samples1)
#
# pylab.subplot(412)
# pylab.title('(received signal 2)')
# pylab.xlabel('Time (s)')
# pylab.ylabel('Sound amplitude')
# pylab.plot(samples2)
#
# pylab.subplot(413)
# pylab.title('(extracted signal 1)')
# pylab.xlabel('Time (s)')
# pylab.ylabel('Sound amplitude')
# pylab.plot(S_[:, 0])
#
# pylab.subplot(414)
# pylab.title('(extracted signal 2)')
# pylab.xlabel('Time (s)')
# pylab.ylabel('Sound amplitude')
# pylab.plot(S_[:, 1])
#
# pylab.subplots_adjust(hspace=.5)
# pylab.savefig('extracted-data.pdf')
# pylab.show()
