import csv
import sys
import traceback
import numpy as np
import pandas as pd
import tensorflow as tf
import librosa

from vggish_input import wavfile_to_examples
from vggish_inference_demo import run_inference
from vggish_inference_demo import writeToDisk

from Data.kaggleKernels.featureExtractorMFCC import extractMFCC

SEPARATOR = "======================================================================"
THIN_SEPARATOR = "----------------------------------------------------------------------"
staticAudioDataDir = "/home/sanket/Documents/Data/SoundData/audio_train"
staticEmbeddingsDir = "/home/sanket/Documents/Data/SoundData/audio_train/vggEncoding/formattedInput/"
featureArrayDir = "/home/sanket/Documents/Data/SoundData/audio_train/featuresDir/"
featureReducedDir = "/home/sanket/Documents/Data/SoundData/audio_train/featuresDir/reducedDataDir/"
staticMFCCDir = "/home/sanket/Documents/Data/SoundData/mfccData/"


def getReducedTrainingSetNames():
    trainFileList = "/home/sanket/Documents/Data/SoundData/reducedDataGuide.txt"
    fileNames = []
    i = 0
    dataGuide = open(trainFileList, "r")
    dataLine = dataGuide.readline()
    while dataLine:
        fileNames.append(dataLine)
        i = i + 1
        dataLine = dataGuide.readline()
    return fileNames


def readEncodedTrainingArrays():
    batchLength = 585
    features = np.empty((batchLength, 128,), dtype=float)
    labels = np.empty((batchLength, 3), dtype=int)
    featuresDir = "/home/sanket/Documents/Data/SoundData/embeddingsDir/"
    labelsDir = "/home/sanket/Documents/Data/SoundData/reducedLabelsSet/"
    # rawAudioFileNames = getRawAudioFileNames()
    rawAudioFileNames = getReducedTrainingSetNames()
    i = 0
    j = 0
    errors = 0
    requiredSize = 0
    while (i < batchLength):
        try:
            fileName = rawAudioFileNames[i]
            fileName = fileName.replace("\n", "")
            featuresFileName = featuresDir + fileName + ".npy"
            featuresFileName = featuresFileName.replace(".wav", ".emb")
            labelsFileName = labelsDir + fileName.replace(".wav", ".lbl")
            labelFile = open(labelsFileName, "r")
            label = labelFile.read().replace("[", "").replace("]", "")
            featuresArr = np.load(featuresFileName)
            labelsArr = np.fromstring(label, dtype=int, sep=',')

            print("Correct file = " + fileName)
            print("Features array shape = " + str(featuresArr.shape))
            print("Labels array shape = " + str(labelsArr.shape))

            k = 0
            while (k < len(featuresArr)):
                features[j] = featuresArr[k]
                print("Debug statement - featuresArr.shape = " + str(featuresArr[k].shape))
                print("Debug statement - labelsArr.shape = " + str(labelsArr.shape))
                labels[j] = labelsArr
                k = k + 1
                j = j + 1

            i = i + 1
        except:
            # print("featuresArr shape = " + str(featuresArr.shape))
            # print("labelsArr shape = " + str(labelsArr.shape))
            # print("Error for file = " + fileName)
            errors = errors + 1
            i = i + 1
            continue
    # print("Num errors = " + str(errors))
    # print("features = " + str(len(features)))
    return features, labels


def getTrainingFileNames():
    fileNames = []
    trainingFile = "/home/sanket/Documents/Data/SoundData/exp2/trainingSplit/trainingFiles.txt"
    i = 0
    dataGuide = open(trainingFile, "r")
    dataLine = dataGuide.readline()
    while dataLine:
        fileNames.append(dataLine)
        i = i + 1
        dataLine = dataGuide.readline()
    return fileNames


def getTestingFileNames():
    fileNames = []
    testingFile = "/home/sanket/Documents/Data/SoundData/exp2/testingSplit/testingFiles.txt"
    i = 0
    dataGuide = open(testingFile, "r")
    dataLine = dataGuide.readline()
    while dataLine:
        fileNames.append(dataLine)
        i = i + 1
        dataLine = dataGuide.readline()
    return fileNames


def getEncodedTrainingFeatures():
    cleanedSet = []
    features = []
    fileNames = getTrainingFileNames()
    i = 0
    while (i < len(fileNames)):
        waveFilePath = (staticAudioDataDir + "/" + fileNames[i]).replace("\n", "")
        print(run_inference(waveFilePath))
        i = i + 1
    return features


def getMFCCTestData():
    exceptionFiles = []
    testFeatures = []
    testLabels = []

    testFileNames = getTestingFileNames()

    testFeatureFilepath = "/home/sanket/Documents/Data/SoundData/audio_train/"
    testLabelsFilepath = "/home/sanket/Documents/Data/SoundData/exp2/testingSplit/testingLabels/"

    i = 0
    while (i < len(testFileNames)):
        currentFileName = testFileNames[i]
        currentFileName = currentFileName.replace("\n", "")
        try:
            currentLabelsFileName = testLabelsFilepath + currentFileName.replace(".wav", ".lbl")
            labelFile = open(currentLabelsFileName, "r")
            label = labelFile.read().replace("[", "").replace("]", "")
            labelsArr = np.fromstring(label, dtype=int, sep=',')

            currentFeatureFileName = testFeatureFilepath + currentFileName

            featuresArr = extractMFCC(currentFeatureFileName)

            numFeatureRows = len(featuresArr)
            j = 0
            while (j < numFeatureRows):
                testFeatures.append(featuresArr[j])
                testLabels.append(labelsArr)
                j = j + 1

            i = i + 1
        except:
            i = i + 1
            exceptionFiles.append(currentFileName)

    testFeaturesFrame = pd.DataFrame(data=testFeatures)
    testLabelsFrame = pd.DataFrame(data=testLabels)
    # print("Number of exceptions = " + str(len(exceptionFiles)) + " content  = " + str(exceptionFiles))

    return (testFeaturesFrame, testLabelsFrame)


def getMFCCTrainData():
    exceptionFiles = []
    trainFeatures = []
    trainLabels = []

    trainFileNames = getTrainingFileNames()

    trainFeatureFilepath = "/home/sanket/Documents/Data/SoundData/audio_train/"
    trainLabelsFilepath = "/home/sanket/Documents/Data/SoundData/exp2/trainingSplit/trainingLabels/"

    i = 0
    while (i < len(trainFileNames)):
        currentFileName = trainFileNames[i]
        currentFileName = currentFileName.replace("\n", "")
        try:
            currentLabelsFileName = trainLabelsFilepath + currentFileName.replace(".wav", ".lbl")
            labelFile = open(currentLabelsFileName, "r")
            label = labelFile.read().replace("[", "").replace("]", "")
            labelsArr = np.fromstring(label, dtype=int, sep=',')

            currentFeatureFileName = trainFeatureFilepath + currentFileName

            featuresArr = extractMFCC(currentFeatureFileName)

            numFeatureRows = len(featuresArr)
            j = 0
            while (j < numFeatureRows):
                trainFeatures.append(featuresArr[j])
                trainLabels.append(labelsArr)
                j = j + 1

            i = i + 1
        except:
            i = i + 1
            exceptionFiles.append(currentFileName)

    trainFeaturesFrame = pd.DataFrame(data=trainFeatures)
    trainLabelsFrame = pd.DataFrame(data=trainLabels)
    # print("Number of exceptions = " + str(len(exceptionFiles)) + " content  = " + str(exceptionFiles))

    return (trainFeaturesFrame, trainLabelsFrame)


def getTrainData():
    exceptionFiles = []
    trainFeatures = []
    trainLabels = []

    trainFileNames = getTrainingFileNames()

    trainFeatureFilepath = "/home/sanket/Documents/Data/SoundData/exp2/masterTrain/"
    trainLabelsFilepath = "/home/sanket/Documents/Data/SoundData/exp2/trainingSplit/trainingLabels/"

    i = 0
    while (i < len(trainFileNames)):
        currentFileName = trainFileNames[i]
        currentFileName = currentFileName.replace("\n", "")
        try:
            currentLabelsFileName = trainLabelsFilepath + currentFileName.replace(".wav", ".lbl")
            labelFile = open(currentLabelsFileName, "r")
            label = labelFile.read().replace("[", "").replace("]", "")
            labelsArr = np.fromstring(label, dtype=int, sep=',')

            currentFeatureFileName = trainFeatureFilepath + currentFileName + ".npy"

            featuresArr = np.load(currentFeatureFileName)

            numFeatureRows = len(featuresArr)
            j = 0
            while (j < numFeatureRows):
                trainFeatures.append(featuresArr[j])
                trainLabels.append(labelsArr)
                j = j + 1

            i = i + 1
        except:
            i = i + 1
            exceptionFiles.append(currentFileName)

    trainFeaturesFrame = pd.DataFrame(data=trainFeatures)
    trainLabelsFrame = pd.DataFrame(data=trainLabels)
    # print("Number of exceptions = " + str(len(exceptionFiles)) + " content  = " + str(exceptionFiles))

    return (trainFeaturesFrame, trainLabelsFrame)


def getTestData():
    exceptionFiles = []
    testFeatures = []
    testLabels = []

    testFileNames = getTestingFileNames()

    testFeatureFilepath = "/home/sanket/Documents/Data/SoundData/exp2/masterTest/"
    testLabelsFilepath = "/home/sanket/Documents/Data/SoundData/exp2/testingSplit/testingLabels/"

    i = 0
    while (i < len(testFileNames)):
        currentFileName = testFileNames[i]
        currentFileName = currentFileName.replace("\n", "")
        try:
            currentLabelsFileName = testLabelsFilepath + currentFileName.replace(".wav", ".lbl")
            labelFile = open(currentLabelsFileName, "r")
            label = labelFile.read().replace("[", "").replace("]", "")
            labelsArr = np.fromstring(label, dtype=int, sep=',')

            currentFeatureFileName = testFeatureFilepath + currentFileName + ".npy"

            featuresArr = np.load(currentFeatureFileName)

            numFeatureRows = len(featuresArr)
            j = 0
            while (j < numFeatureRows):
                testFeatures.append(featuresArr[j])
                testLabels.append(labelsArr)
                j = j + 1

            i = i + 1
        except:
            i = i + 1
            exceptionFiles.append(currentFileName)

    testFeaturesFrame = pd.DataFrame(data=testFeatures)
    testLabelsFrame = pd.DataFrame(data=testLabels)
    # Meow,Bark,Cowbell,Harmonica,Clarinet,Applause
    # testLabelsFrame.columns = ['Meow', 'Bark', 'Cowbell', 'Harmonica', 'Clarinet', 'Applause']
    # testLabelsFrame.columns = ['Meow', 'Bark', 'Cowbell', 'Clarinet', 'Telephone', 'Drawer_open_or_close',
    #                            'Computer_keyboard', 'Flute']
    # # print("Number of exceptions = " + str(len(exceptionFiles)) + " content  = " + str(exceptionFiles))

    return (testFeaturesFrame, testLabelsFrame)


# (testFeatures,testLabels) = getTestData()
# print(str(testFeatures))
# print(str(testLabels))


# (trainFeatures, trainLabels) = getTrainData()
# trainFeaturesFrame = pd.DataFrame(data=trainFeatures)
# trainLabelsFrame = pd.DataFrame(data=trainLabels)
#
# print(trainFeaturesFrame)
# print(trainLabelsFrame)
# # print("Num features = " + str(len(trainFeatures)) + " Num Labels = " + str(len(trainLabels)))
#
# getTestData()

def getTrainingMFCCData():
    trainMfccData = []
    trainMfccLabels = []
    trainingFileNames = getTrainingFileNames()
    i = 0
    while (i < len(trainingFileNames)):
        currentFile = trainingFileNames[i]
        mfccTrainFile = staticMFCCDir + currentFile
        mfccTrainFile = mfccTrainFile.replace("\n", "")
        mfccTrainFile = mfccTrainFile + ".npy"

        try:
            mfccTrainArr = np.load(mfccTrainFile)

            currentLabelsFileName = "/home/sanket/Documents/Data/SoundData/exp2/trainingSplit/trainingLabels/" + currentFile.replace(
                ".wav", ".lbl")
            currentLabelsFileName = currentLabelsFileName.replace("\n", "")
            labelFile = open(currentLabelsFileName, "r")
            label = labelFile.read().replace("[", "").replace("]", "")
            labelsArr = np.fromstring(label, dtype=int, sep=',')
            trainMfccData.append(mfccTrainArr)
            trainMfccLabels.append(labelsArr)
        except:
            print("Filename threw an exception :: " + mfccTrainFile)
        i = i + 1

    trainFeaturesFrame = pd.DataFrame(data=trainMfccData)
    trainLabelsFrame = pd.DataFrame(data=trainMfccLabels)

    # trainFeaturesFrameNormal = (trainFeaturesFrame - trainFeaturesFrame.mean()) / (
    #         trainFeaturesFrame.max() - trainFeaturesFrame.min())
    # print("Number of exceptions = " + str(len(exceptionFiles)) + " content  = " + str(exceptionFiles))

    return (trainFeaturesFrame, trainLabelsFrame)


def getTestingMFCCData():
    testMfccData = []
    testMfccLabels = []
    testingFileNames = getTestingFileNames()
    i = 0
    while (i < len(testingFileNames)):
        currentFile = testingFileNames[i]
        mfccTestFile = staticMFCCDir + currentFile
        mfccTestFile = mfccTestFile.replace("\n", "")
        mfccTestFile = mfccTestFile + ".npy"

        try:
            mfccTestArr = np.load(mfccTestFile)

            currentLabelsFileName = "/home/sanket/Documents/Data/SoundData/exp2/testingSplit/testingLabels/" + currentFile.replace(
                ".wav", ".lbl")
            currentLabelsFileName = currentLabelsFileName.replace("\n", "")
            labelFile = open(currentLabelsFileName, "r")
            label = labelFile.read().replace("[", "").replace("]", "")
            labelsArr = np.fromstring(label, dtype=int, sep=',')
            testMfccData.append(mfccTestArr)
            testMfccLabels.append(labelsArr)
        except:
            print("Filename threw an exception :: " + mfccTestFile)
        i = i + 1

    testFeaturesFrame = pd.DataFrame(data=testMfccData)
    testLabelsFrame = pd.DataFrame(data=testMfccLabels)

    # testFeaturesFrameNormal = (testFeaturesFrame - testFeaturesFrame.mean()) / (
    #         testFeaturesFrame.max() - testFeaturesFrame.min())
    return (testFeaturesFrame, testLabelsFrame)

def prepare_data(df, config, data_dir):
    X = np.empty(shape=(df.shape[0], config.dim[0], config.dim[1], 1))
    input_length = config.audio_length
    for i, fname in enumerate(df.index):
        print(fname)
        try:
            file_path = str(data_dir) + str(fname)
            data, _ = librosa.core.load(file_path, sr=config.sampling_rate, res_type="kaiser_fast")

            # Random offset / Padding
            if len(data) > input_length:
                max_offset = len(data) - input_length
                offset = np.random.randint(max_offset)
                data = data[offset:(input_length+offset)]
            else:
                if input_length > len(data):
                    max_offset = input_length - len(data)
                    offset = np.random.randint(max_offset)
                else:
                    offset = 0
                data = np.pad(data, (offset, input_length - len(data) - offset), "constant")

            data = librosa.feature.mfcc(data, sr=config.sampling_rate, n_mfcc=config.n_mfcc)
            data = np.expand_dims(data, axis=-1)
            X[i,] = data
        except:
            continue
    return X

# trainFeatures, trainLabels = getTrainingMFCCData()
# print(str(trainFeatures))
# print("*"*80)
# print(str(trainLabels))
# print("*"*80)
