#!/usr/bin/env python
from testAudioExtraction import getRawAudioFileNames

import numpy as np
import essentia.standard as es

staticAudioPath = "/home/sanket/Documents/Data/SoundData/audio_train/"
staticSavePath = "/home/sanket/Documents/Data/SoundData/mfccData/"

def displayFeatures(features):
    numericalFeatures = np.zeros(17)
    numericalFeatures[0] = features['metadata.audio_properties.replay_gain']
    numericalFeatures[1] = features['lowlevel.loudness_ebu128.integrated']
    numericalFeatures[2] = features['lowlevel.loudness_ebu128.loudness_range']
    numericalFeatures[3] = features['rhythm.bpm']

    k=0
    while(k < len(features['lowlevel.mfcc.mean'])):
        numericalFeatures[4+k] = features['lowlevel.mfcc.mean'][k]
        k = k + 1

    print("Filename:", features['metadata.tags.file_name'])
    print("-" * 80)
    print("Replay gain:", features['metadata.audio_properties.replay_gain'])
    print("EBU128 integrated loudness:", features['lowlevel.loudness_ebu128.integrated'])
    print("EBU128 loudness range:", features['lowlevel.loudness_ebu128.loudness_range'])
    print("-" * 80)
    print("MFCC mean:", features['lowlevel.mfcc.mean'])
    print("MFCC mean:shape", features['lowlevel.mfcc.mean'].shape)
    print("-" * 80)
    print("BPM:", features['rhythm.bpm'])
    print("Beat positions (sec.)", features['rhythm.beats_position'])
    print("-" * 80)
    print("Key/scale estimation (using a profile specifically suited for electronic music):",
          features['tonal.key_edma.key'], features['tonal.key_edma.scale'])

    return numericalFeatures


# (rate, sig) = wav.read("/home/sanket/Documents/Data/SoundData/audio_train/0a15b36b.wav")
# mfcc_feat = mfcc(sig, rate)
# d_mfcc_feat = delta(mfcc_feat, 2)
# fbank_feat = logfbank(sig, rate)

# print(fbank_feat[1:3,:])
#
# print(str(fbank_feat))
#
# print(str(fbank_feat.shape))

trainingFiles = getRawAudioFileNames()
i = 0
while (i < len(trainingFiles)):
    currentFile = trainingFiles[i];
    saveFileName = staticSavePath + currentFile
    currentFile = staticAudioPath + currentFile
    print(currentFile)
    try:
        features, features_frames = es.MusicExtractor(lowlevelStats=['mean', 'stdev'],
                                                      rhythmStats=['mean', 'stdev'],
                                                      tonalStats=['mean', 'stdev'])(currentFile)
        print("=============== Features ======================== ")
        numericalFeatures = displayFeatures(features)
        print("*************************************************************")
        print(str(numericalFeatures))
        np.save(saveFileName, numericalFeatures)
        i = i + 1
    except:
        i = i + 1









































































































    # (rate, sig) = wav.read(currentFile)
    # mfcc_feat = mfcc(sig, rate)
    # d_mfcc_feat = delta(mfcc_feat, 2)
    # fbank_feat = logfbank(sig, rate)
    # print(str(fbank_feat.shape) + " for i = " + str(i))
    # j = 0
    # k = 0
    # while(j < len(fbank_feat)):
    #     print(fbank_feat[j])
    #     j = j + 1
    #
    # print("=============================================================")
