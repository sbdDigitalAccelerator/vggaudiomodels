import os
import wave
import pylab
import matplotlib

import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
from scipy.io import wavfile
from scipy import vstack

from sklearn.decomposition import FastICA, PCA

STATIC_AUDIO_PATH = "/home/sanket/Documents/Data/skanska/mlSounds/"

INPUT_FILE = "hammerDrill.wav"
OUTPUT_FILE_1 = "hammer.wav"
OUTPUT_FILE_2 = "drill.wav"

input_rate1, inputArr = wavfile.read(STATIC_AUDIO_PATH + INPUT_FILE)

print('input_rate1', input_rate1)
print(str(inputArr.shape))
print("=" * 50)
print(inputArr)

ica = FastICA(n_components=2)
S_ = ica.fit_transform(inputArr.reshape(-1,1))

print('original signal=', inputArr)
print('recovered signal=', S_)
print('extracted signal1', S_[:, 0])
print('extracted signal2', S_[:, 1])

