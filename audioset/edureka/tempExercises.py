import tensorflow as tf

node1 = tf.constant(2.0, tf.float32)
node2 = tf.constant(3.0, tf.float32)

c = node1 * node2

print(node1)
print(node2)

with tf.Session() as sess:
    output = sess.run([c])
    print(output)
    File_Writer = tf.summary.FileWriter("/home/sanket/Documents/workspace/models/research/audioset/Data/graph", sess.graph)

